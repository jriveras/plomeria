<?php

class cliente{
    private $id;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $estado;
    private $codigo_activacion;
    private $conexion;
    private $clienteDAO;
    
    /**
     * @return
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    
    /**
     * @return string
     */
    public function getApellido()
    {
        return $this->apellido;
    }
    
    /**
     * @return string
     */
    public function getCorreo()
    {
        return $this->correo;
    }
    
    /**
     * @return string
     */
    public function getClave()
    {
        return $this->clave;
    }
    
    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }
    
    /**
     * @return mixed
     */
    public function getCodigo_activacion()
    {
        return $this->codigo_activacion;
    }
    
    public function Cliente($id=0, $nombre="", $apellido="", $correo="", $clave="", $estado="", $codigo_activacion="") {
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> estado = $estado;
        $this -> codigo_activacion = $codigo_activacion;
        $this -> Conexion = new conexion();
        $this -> ClienteDAO = new clienteDAO($this -> id, $this -> nombre, $this -> apellido, $this -> correo, $this -> clave, $this -> estado, $this -> codigo_activacion);
    }
    
    public function crear(){
        $this -> Conexion -> abrir();
        $this -> Conexion -> ejecutar($this -> ClienteDAO -> crear());
        $this -> Conexion -> ejecutar($this -> ClienteDAO -> consultarUltimoId());
        $resultado = $this -> Conexion -> extraer();
        $this -> Conexion -> cerrar();
        return $resultado[0];
    }
    
    public function existeCorreo(){
        $this -> Conexion -> abrir();
        $this -> Conexion -> ejecutar($this -> ClienteDAO -> existeCorreo());
        $resultado = $this -> Conexion -> extraer();
        $this -> Conexion -> cerrar();
        return ($resultado[0]==0)?false:true;
    }
    
    public function existeCodigoActivacion(){
        $this -> Conexion -> abrir();
        $this -> Conexion -> ejecutar($this -> ClienteDAO -> existeCodigoActivacion());
        $resultado = $this -> Conexion -> extraer();
        $this -> Conexion -> cerrar();
        return ($resultado[0]==0)?false:true;
    }
    
    
    public function autenticar(){
        $this -> Conexion -> abrir();
        echo $this -> ClienteDAO -> autenticar();
        $this -> Conexion -> ejecutar($this -> ClienteDAO -> autenticar());
        if($this -> Conexion -> numFilas() == 0){
            return false;
        }else{
            $resultado = $this -> Conexion -> extraer();
            $this -> id = $resultado[0];
            $this -> estado = $resultado[1];
            return true;
        }
    }
    
    
    public function consultar(){
        $this -> Conexion -> abrir();
        $this -> Conexion -> ejecutar($this -> ClienteDAO -> consultar());
        $this -> Conexion -> cerrar();
        $datos = $this -> Conexion -> extraer();
        $this -> nombre = $datos[0];
        $this -> apellido = $datos[1];
        $this -> correo = $datos[2];
    }
    
    public function activar(){
        $this -> Conexion -> abrir();
        $this -> Conexion -> ejecutar($this -> ClienteDAO -> activar());
        $this -> Conexion -> cerrar();
    }
    
    public function deshabilitar(){
        $this -> Conexion -> abrir();
        $this -> Conexion -> ejecutar($this -> ClienteDAO -> deshabilitar());
        $this -> Conexion -> cerrar();
    }
    
    public function consultarTodos(){
        $this -> Conexion -> abrir();
        $this -> Conexion -> ejecutar($this -> ClienteDAO -> consultarTodos());
        $clientes = array();
        while(($resultado = $this -> Conexion -> extraer()) != null){
            array_push($clientes, new Cliente($resultado[0], $resultado[1], $resultado[2], $resultado[3], "", $resultado[4]));
        }
        $this -> Conexion -> cerrar();
        return $clientes;
    }
    
    public function consultarEstado(){
        $this -> Conexion -> abrir();
        $this -> Conexion -> ejecutar($this -> ClienteDAO -> consultarEstado());
        $resultado = $this -> Conexion -> extraer();
        $this -> estado = $resultado[0];
    }
    
}
?>
